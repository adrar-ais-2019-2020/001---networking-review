Network review
==============

Le but est de créés trois réseaux séparer par des VLAN comprenant deux bureaux et une salle serveur:

.. image:: ../images/network_review.svg

Inter-VLAN
==========

Switch
------

.. code:: terminal
  
  Switch>en
  Switch#conf t
  Enter configuration commands, one per line.  End with CNTL/Z.
  Switch(config)#vlan 10
  Switch(config-vlan)#name vlan10
  Switch(config-vlan)#exit
  Switch(config)#vlan 20
  Switch(config-vlan)#name vlan20
  Switch(config-vlan)#exit
  witch(config)#vlan 90
  Switch(config-vlan)#name vlan90
  Switch(config-vlan)#exit
  Switch(config)#interface range fastEthernet 0/1-10
  Switch(config-if-range)#switchport access vlan 10
  Switch(config-if-range)#no shutdown 
  Switch(config-if-range)#exit
  Switch(config)#interface range fastEthernet 0/11-20
  witch(config-if-range)#switchport access vlan 20
  Switch(config-if-range)#no shutdown 
  Switch(config-if-range)#exit
  Switch(config)#interface range fastEthernet 0/21-23
  witch(config-if-range)#switchport access vlan 90
  Switch(config-if-range)#no shutdown 
  Switch(config-if-range)#exit
  Switch#copy running-config startup-config 
  Destination filename [startup-config]? 
  Building configuration...
  [OK]
  Switch#conf t
  Enter configuration commands, one per line.  End with CNTL/Z.
  Switch(config)#interface GigabitEthernet 0/1
  Switch(config-if)#switchport mode trunk
  Switch(config-if)#switchport trunk allowed vlan 10,20,90
  Switch(config-if)#no shutdown
  Switch(config-if)#end
  Switch#copy running-config startup-config 
  Destination filename [startup-config]? 
  Building configuration...
  [OK]


Router1
-------

.. code:: terminal

  Router>en
  Router#show vlan

  VLAN Name                             Status    Ports
  ---- -------------------------------- --------- -------------------------------
  1    default                          active    
  1002 fddi-default                     act/unsup 
  1003 token-ring-default               act/unsup 
  1004 fddinet-default                  act/unsup 
  1005 trnet-default                    act/unsup 
  
  VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
  ---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
  1    enet  100001     1500  -      -      -        -    -        0      0
  1002 fddi  101002     1500  -      -      -        -    -        0      0   
  1003 tr    101003     1500  -      -      -        -    -        0      0   
  1004 fdnet 101004     1500  -      -      -        ieee -        0      0   
  1005 trnet 101005     1500  -      -      -        ibm  -        0      0   
  
  Remote SPAN VLANs
  ------------------------------------------------------------------------------
  
  
  Primary Secondary Type              Ports
  ------- --------- ----------------- ------------------------------------------
  Router#conf t
  Enter configuration commands, one per line.  End with CNTL/Z.
  Router(config)#interface fastEthernet 0/0
  Router(config-if)#no shutdown 
  
  Router(config-if)#
  %LINK-5-CHANGED: Interface FastEthernet0/0, changed state to up
  
  %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
  Router(config-if)#exit
  Router(config)#interface fastEthernet 0/0.10
  Router(config-subif)#
  %LINK-5-CHANGED: Interface FastEthernet0/0.10, changed state to up
  
  %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0.10, changed state to up
  
  Router(config-subif)#encapsulation dot1Q 10
  Router(config-subif)#ip address 192.168.1.254 255.255.255.0
  Router(config-subif)#ip helper-address 192.168.90.1
  Router(config-subif)#no shutdow
  Router(config-subif)#exit
  outer(config)#interface fastEthernet 0/0.20
  Router(config-subif)#
  %LINK-5-CHANGED: Interface FastEthernet0/0.20, changed state to up
  
  %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0.20, changed state to up
  
  Router(config-subif)#encapsulation dot1Q 20
  Router(config-subif)#ip address 192.168.2.254 255.255.255.0
  Router(config-subif)#ip helper-address 192.168.90.1
  Router(config-subif)#no shutdow
  Router(config-subif)#exit
  outer(config)#interface fastEthernet 0/0.90
  Router(config-subif)#
  %LINK-5-CHANGED: Interface FastEthernet0/0.90, changed state to up
  
  %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0.90, changed state to up
  
  Router(config-subif)#encapsulation dot1Q 90
  Router(config-subif)#ip address 192.168.90.254 255.255.255.0
  Router(config-subif)#no shutdow
  Router(config-subif)#exit
  Router(config)#router rip
  Router(config-router)#net
  Router(config-router)#version 2
  Router(config-router)#network 192.168.1.0
  Router(config-router)#network 192.168.2.0
  Router(config-router)#network 192.168.90.0
  Router#copy running-config startup-config 
  Destination filename [startup-config]? 
  Building configuration...
  [OK]

.. code:: shell

  Router(config)#ip access-list extended <rule-name>
  Router(config-ext-nacl)#permit tcp 192.168.10.0 0.0.0.255 host 192.168.30.1 eq 80
  Router(config-ext-nacl)#permit tcp <authorised network> <authorized network reverse subnet> host <destination ip eq <destination port>
  Router(config-ext-nacl)#deny udp 192.168.20.254 host 192.168.90.1 eq 67
  Router(config-ext-nacl)#deny udp <interface source a bloquer> host <adresse de destination> eq 67
  Router(config-ext-nacl)#exit
  Router(config)#interface fastEthernet 0/0.30
  Router(config-subif)#access-group acces-srv-web out
  Router(config)#show acces-list 
  Router(config)#no ip access-list extended <rule-name>


Les ACL sont appliquer sur l'interface la plus proche du service a fournir.
  

Configuration du réseau sur le serveur Linux OpenSUSE
-----------------------------------------------------

Sur OpenSUSE, il y a trois fichiers à modifier :

* :emphasis:`/etc/sysconfig/network/ifcfg-ethX` ou :emphasis:`ethX` correspond à l'interface réseau à modifier
* :emphasis:`/etc/sysconfig/network/routes`
* :emphasis:`/etc/sysconfig/network/config`

Dans un premier temps on attribue une adresse IP fixe dans le fichier :emphasis:`/etc/sysconfig/network/ifcfg-eth0` :

.. code:: terminal

  IPADDR='192.0.2.1/24'         # Défini l'adresse IPv4 et sont CIDR
  BOOTPROTO='static'            # Défini que l'interface a un adressage statique
  STARTMODE='auto'              # Défini que l'interface s'active au démarrage dès que possible

On ajoute la route route par défaut dans :emphasis:`/etc/sysconfig/network/routes` :

.. code:: terminal

  # Destination   Gateway         Netmask         Interface
  
  # IPv4
  127.0.0.0/8     -               -               lo
  default         192.0.2.254     -               eth0

.. warning::

  La colonne :emphasis:`Netmask` doit toujours contenir un tiret :emphasis:`-`, la notation CIDR est utilisé et l'usage du netmask complet est déprécié

Pour finir la configuration des DNS on modifie le fichier de configuration :emphasis:`/etc/sysconfig/network/config` :

.. code:: terminal

  ## Type:        string
  ## Default:     ""
  #
  # List of DNS nameserver IP addresses to use for host-name lookup.
  # When the NETCONFIG_DNS_FORWARDER variable is set to "resolver",
  # the name servers are written directly to /etc/resolv.conf.
  # Otherwise, the nameserver are written into a forwarder specific
  # configuration file and the /etc/resolv.conf does not contain any
  # nameservers causing the glibc to use the name server on the local
  # machine (the forwarder). See also netconfig(8) manual page.
  #
  NETCONFIG_DNS_STATIC_SERVERS="192.0.2.250"

Une fois la modification effectuée, il faut exécuter la commande :code:`sudo netconfig update -f` pour mettre à jour les informations, par exemple l'ajout des adresses des serveurs de noms dans :emphasis:`/etc/resolv.conf`.

Installation et configuration de PostgreSQL
-------------------------------------------

L'installation de PostgreSQL sous OpenSUSE 15.1 est simple, dans un premier temps on installe les paquets :

.. code:: shell

  ~> sudo  zypper install postgresql postgresql-server postgresql-contrib

On rend le service active immédiatement et persistant au démarrage de la machine :

.. code:: shell

  ~> sudo systemctl enable postgresql.service
  ~> sudo systemctl start postgresql.service
  ~> systemctl status postgresql.service
  ● postgresql.service - PostgreSQL database server
     Loaded: loaded (/usr/lib/systemd/system/postgresql.service; enabled; vendor preset: disabl>
     Active: active (running) since Thu 2019-10-17 12:15:37 CEST; 2min 44s ago
    Process: 2332 ExecStart=/usr/share/postgresql/postgresql-script start (code=exited, status=>
    Main PID: 2355 (postgres)
       Tasks: 8 (limit: 4915)
     CGroup: /system.slice/postgresql.service
             ├─2355 /usr/lib/postgresql10/bin/postgres -D /var/lib/pgsql/data
             ├─2356 postgres: logger process   
             ├─2358 postgres: checkpointer process   
             ├─2359 postgres: writer process   
             ├─2360 postgres: wal writer process   
             ├─2361 postgres: autovacuum launcher process   
             ├─2362 postgres: stats collector process   
             └─2363 postgres: bgworker: logical replication launcher

Par défaut, PostgreSQL ne fait confiance à personne, il est nécessaire de se connecter au compte de `postgres` pour modifier le mot de passe et ajouter un nouvelle utilisateur.

.. code:: shell

  user~> sudo su - postgres
  postgres~> psql
  postgres=# \password 
  Enter new password: 
  Enter it again:

Création de l'utilisateur :

.. code:: shell

  user~> sudo su - postgres
  postgres~> createuser --interactive
  Enter name of role to add: rollniak
  Shall the new role be a superuser? (y/n) y 


Installation du serveur DNS
---------------------------

On installe les paquets nécessaire au fonctionnement du serveur Bind

.. code:: shell

  ~> sudo zypper in bind bind-doc bind-utils                                
  Loading repository data...                                                                    
  Reading installed packages...                                                                 
  'bind-utils' is already installed.                                                            
  No update candidate for 'bind-utils-9.11.2-lp151.11.6.1.x86_64'. The highest available version
   is already installed.                                                                        
  Resolving package dependencies...                                                            
                                                                                               
  The following 4 NEW packages are going to be installed:                                    
    bind bind-chrootenv bind-doc libmariadb3                                                   
                                                                                               
  4 new packages to install.                                                                   
  Overall download size: 2.1 MiB. Already cached: 0 B. After the operation, additional 9.6 MiB  
  will be used.                                                                                
  Continue? [y/n/v/...? shows all options] (y):                                                
  Retrieving package bind-chrootenv-9.11.2-lp151.11.6.1.x86_64                                 
                                                           (1/4),  39.6 KiB (  1.6 KiB unpacked$
  Retrieving: bind-chrootenv-9.11.2-lp151.11.6.1.x86_64.rpm ..............................[done$
  Retrieving package bind-doc-9.11.2-lp151.11.6.1.noarch   (2/4),   1.6 MiB (  8.3 MiB unpacked$
  Retrieving: bind-doc-9.11.2-lp151.11.6.1.noarch.rpm .......................[done (13.8 KiB/s)$
  Retrieving package libmariadb3-3.1.2-lp151.3.3.1.x86_64  (3/4), 127.1 KiB (350.9 KiB unpacked$
  Retrieving: libmariadb3-3.1.2-lp151.3.3.1.x86_64.rpm ...................................[done$
  Retrieving package bind-9.11.2-lp151.11.6.1.x86_64       (4/4), 363.0 KiB (948.1 KiB unpacked$
  Retrieving: bind-9.11.2-lp151.11.6.1.x86_64.rpm ...........................[done (15.3 KiB/s)$
  Checking for file conflicts: ...........................................................[done$
  (1/4) Installing: bind-chrootenv-9.11.2-lp151.11.6.1.x86_64 ............................[done$
  Additional rpm output:                                                                       
  Updating /etc/sysconfig/named ...                                                            
  Updating /etc/sysconfig/syslog ...                                                           
                                                                                               
                                                                                               
  (2/4) Installing: bind-doc-9.11.2-lp151.11.6.1.noarch ..................................[done$
  (3/4) Installing: libmariadb3-3.1.2-lp151.3.3.1.x86_64 .................................[done$
  (4/4) Installing: bind-9.11.2-lp151.11.6.1.x86_64 ......................................[done$
  Additional rpm output:                                                                       
  Updating /etc/sysconfig/named ... 

On change le nom de la machine en ns1

.. code:: terminal

  ~> sudo hostnamectl set-hostname ns1

On édit le fichier de configuration :emphasis:`/etc/sysconfig/config` est on modifie les lignes suivante :

.. code:: terminal


  ## Type:        string
  ## Default:     ""
  #
  # List of DNS domain names used for host-name lookup.
  # It is written as search list into the /etc/resolv.conf file.
  #
  NETCONFIG_DNS_STATIC_SEARCHLIST="ais.local"

.. code:: terminal

  ## Type:        string
  ## Default:     ""
  #
  # List of DNS nameserver IP addresses to use for host-name lookup.
  # When the NETCONFIG_DNS_FORWARDER variable is set to "resolver",
  # the name servers are written directly to /etc/resolv.conf.
  # Otherwise, the nameserver are written into a forwarder specific
  # configuration file and the /etc/resolv.conf does not contain any
  # nameservers causing the glibc to use the name server on the local
  # machine (the forwarder). See also netconfig(8) manual page.
  #
  NETCONFIG_DNS_STATIC_SERVERS="127.0.0.1"

Après modifications, on exécute la commande :code:`sudo netconfig update -f` pour appliquer les changements, on peut vérifier les changements comme ceci :

.. code:: terminal

  ~> cat /etc/resolv.conf 
  ### /etc/resolv.conf is a symlink to /var/run/netconfig/resolv.conf
  ### autogenerated by netconfig!
  #
  # Before you change this file manually, consider to define the
  # static DNS configuration using the following variables in the
  # /etc/sysconfig/network/config file:
  #     NETCONFIG_DNS_STATIC_SEARCHLIST
  #     NETCONFIG_DNS_STATIC_SERVERS
  #     NETCONFIG_DNS_FORWARDER
  # or disable DNS configuration updates via netconfig by setting:
  #     NETCONFIG_DNS_POLICY=''
  #
  # See also the netconfig(8) manual page and other documentation.
  #
  ### Call "netconfig update -f" to force adjusting of /etc/resolv.conf.
  search ais.local
  nameserver 127.0.0.1

Une fois la configuration terminé, il est nécessaire de démarrer le service `named` :

.. code:: terminal

  ~> sudo systemctl start named.service 
  ~> sudo systemctl enable named.service 
  Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
  rollniak@linux-mof0:~> sudo systemctl status named
  ● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2019-10-17 22:03:05 CEST; 11s ago
   Main PID: 4706 (named)
      Tasks: 5 (limit: 4915)
     CGroup: /system.slice/named.service
             └─4706 /usr/sbin/named -t /var/lib/named -u named
  
  Oct 17 22:03:05 ns1 named[4706]: command channel listening on 127.0.0.1#953
  Oct 17 22:03:05 ns1 named[4706]: configuring command channel from '/etc/rndc.key'
  Oct 17 22:03:05 ns1 named[4706]: command channel listening on ::1#953
  Oct 17 22:03:05 ns1 named[4706]: managed-keys-zone: loaded serial 0
  Oct 17 22:03:05 ns1 named[4706]: zone localhost/IN: loaded serial 42
  Oct 17 22:03:05 ns1 named[4706]: zone 0.0.127.in-addr.arpa/IN: loaded serial 42
  Oct 17 22:03:05 ns1 named[4706]: zone 0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0>
  Oct 17 22:03:05 ns1 systemd[1]: Started Berkeley Internet Name Domain (DNS).
  Oct 17 22:03:05 ns1 named[4706]: all zones loaded
  Oct 17 22:03:05 ns1 named[4706]: running

Sous OpenSUSE, on ajoute les zones dans le fichier `/etc/named.conf.include` :

.. code:: terminal

  zone "internal.private" in {
      type master;
      file "master/internal.private.zone"
  }

Par défaut sous OpenSUSE le répertoire de travail est dans `/var/lib/named` on crée un fichier `/var/lib/named/master/internal.private.zone`.

Pour cela on part d'un fichier de configuration :

On crée une zone dans `/etc/named.conf.include` :

.. code:: terminal

  zone "internal.private" in {
      type master;
      file "master/internal.private.zone";
  };

.. code:: terminal

  ~> sudo cp /usr/share/doc/packages/bind/config/master/example.net.zone /var/lib/named/master/internal.private.zone


.. code:: temrinal

  $TTL 2D
  internal.private.  IN SOA  ns1  ns1.ais.local. (                                             
                                  2019101801      ; serial                                     
                                  1D              ; refresh                                    
                                  2H              ; retry                                      
                                  1W              ; expiry                                     
                                  2D )            ; minimum                                    
  
                  IN NS           ns1
                  IN MX           10 ns1
  
                  IN A            192.0.2.250
  ns1             IN A            192.0.2.250
  
  www             IN CNAME        internal.private.                                            
  ftp             IN CNAME        internal.private.

Il est nécessaire de recharger la configuration et d'ouvrir le port 53 :

.. code:: terminal

  ~> sudo systemctl reload named
  ~> sudo firewall-cmd --permanent --add-port=53/tcp
  ~> sudo firewall-cmd --permanent --add-port=53/udp


Le serveur Web
--------------

Pour le serveur Web, on installe NGINX. Sous OpenSUSE on fait comme suis :

.. code::

  ~> sudo zypper in nginx
  [sudo] password for root:
  Loading repository data...
  Reading installed packages...
  Resolving package dependencies...

  The following 2 NEW packages are going to be installed:
    nginx vim-plugin-nginx

  The following recommended package was automatically selected:
    vim-plugin-nginx

  2 new packages to install.
  Overall download size: 849,1 KiB. Already cached: 0 B. After the operation, additional 2,7 MiB will be
  used.
  Continue? [y/n/v/...? shows all options] (y):
  Retrieving package vim-plugin-nginx-1.14.2-lp151.4.3.1.noarch    (1/2),  68,9 KiB (124,5 KiB unpacked)
  Retrieving: vim-plugin-nginx-1.14.2-lp151.4.3.1.noarch.rpm .....................................[done]
  Retrieving package nginx-1.14.2-lp151.4.3.1.x86_64               (2/2), 780,2 KiB (  2,6 MiB unpacked)
  Retrieving: nginx-1.14.2-lp151.4.3.1.x86_64.rpm ..................................[done (116,1 KiB/s)]
  Checking for file conflicts: ...................................................................[done]
  (1/2) Installing: vim-plugin-nginx-1.14.2-lp151.4.3.1.noarch ...................................[done]
  (2/2) Installing: nginx-1.14.2-lp151.4.3.1.x86_64 ..............................................[done

Pour rendre le service actif immédiatement et persistant après le redémarrage il est nécessaire de faire les commandes suivantes :

.. code:: terminal

  ~> systemctl start nginx
  ~> systemctl enable nginx


On vérifie que le service est actif via la commande :code:`curl` :

.. code:: terminal

  ~> curl localhost
  <html>
  <head><title>403 Forbidden</title></head>
  <body bgcolor="white">
  <center><h1>403 Forbidden</h1></center>
  <hr><center>nginx/1.14.2</center>
  </body>
  </html

On a un retour de NGINX, il est opérationnel, les ports du pare-feu sont actuellement fermés pour le monde extérieur et la page est accessible uniquement en :emphasis:`localhost`.

Avant d'ouvrir les ports il est conseillé de mettre en place un site par défaut, pour cela on va créer un hôte virtuel.

Création d'un hôte virtuelle sous NGINX
=======================================

Il faut crée le répertoire qui accueillera le fichier HTML et y ajouter du contenu :

.. code:: terminal

  sudo mkdir /srv/www/target.tld
  echo "Bienvenue sur target.tld" | sudo tee /srv/www/target.tld/index.html

.. note::
  Sous OpenSUSE, le chemin par défaut pour les serveurs web se situe dans :emphasis:`/srv/www`

L'ajout d'un hôte virtuel s'effectue en créant un fichier de configuration, par exemple :emphasis:`/etc/nginx/vhost.d/target.tld.conf` :

.. code:: terminal

  server {
          listen  80 default_server;
          server_name target.tld;

          location / {
                  root /srv/www/target.tld/;
                  index index.html index.htm;
          }
  }

.. note::
  L'option default_server est présente que sur un seul hôte virtuel.

Il est nécessaire par la suite de vérifier la conformité du fichier de configuration et ensuite recharger les fichiers de configuration. En cas de retour valide, on recharge la configuration de NGINX sans arrêter le service :

.. code:: terminal

  ~> sudo nginx -t
  ~> sudo systemctl reload nginx

OpenSUSE utilise Firewalld pour gérer les règles de pare-feu. Pour ouvrir le port 80 on utilise la commande comme suit :

.. code:: terminal

  ~> sudo firewall-cmd --permanent --add-port=80/tcp
  ~> sudo firewall-cmd --reload

