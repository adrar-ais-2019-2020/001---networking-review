===================
Révisions des bases des réseaux
===================

Etude de cas
============

Dans le cadre des révisions des bases de réseaux au sein de la classe Administrateur(trice) d'infrastructure sécurisée, on fait une étude de cas pour une collectivité qui gere 47 restaurant scolaires.

Ces restaurants sont groupes en cinq secteurs, supervises par des responsables e secteur, charges de la gestion pratique des restaurants et de l'organisation des equipes. Les responsables de secteur disposent chacun d'un ordinateur utilise pour des travaux de bureautique. Ces responsables de secteur sont situes dans un lcal distant de l'hotel de ville, batiment principal de la mairie.


La gestion administrative de ces restaurants scolaire est assuree par le "Service des Affaires Generales". Ce service, situe dans les locaux de la mairie, s'occupe ainsi de la gestion du personnel et de l'etablissement du planning des equipes. Il place les differents agents a sa disposition, soit comme comme agents d'entrtien dans les ecoles, soit come agents de restauration dans les restaurants scolaires, soit comme polyvalents s'il s''occupe des deux fontions.


Monsieur Franck Dubois, attache administratif au "Service des Affaires Generales", veut interconnecter le reseaux principal de la mairie au reseaux des responsablesde secteur afin d'ameliorer l'organisation et la gestion administrative du personnel.

Mais cette nouvele liaison necessite l'installation de nouveaux materiels et des modifcations sur la configurations du reseaux existant.

M. Dubois veut profiter de cette evolution pour ameliorer la circulation des flux d'information entre les services concernes, les ecoles et les restaurants scolaires.

Etude du reseau
===============

Comme le reseau des responsables de secteur doit etre integrer au reseau principal de la mairie, il est necessaire de connaitre prealablement l'existant.

La mairie de L. dispose depuis 1999 d'un reseau Ethernet a 100 Mbits/s dont le schema est fourni ci-dessous :

.. image:: ../images/web-ig-ecole-L.svg

Le reseau principal de la mairie est 172.30.16.0/20, tout le reseau principal de la mairie entre dans ce reseau et utilise le CIDR /20.
La plage ip dedier aux hotes est compris entre 172.30.16.1 et 172.30.31.254, en consequence de quoi les machines au rez de chausees peuvent acceder au serveur principal. Actuellement le reseaux principal peut supporter jusqu'a 2^12 soit 4096 machines.

.. tip::
  Le meilleur moyen de calculer une addresse de reseau et de convertire la plus petit addresse IP en binaire et de faire un "ET"(&&) logique avec le masque de sous reseau.

  +--------------------+-------------------------------------+----------------------------+
  | addresse du reseau |        conversion en binaire        |                            |
  |                    |         128 64 32 16 8 4 2 1        |                            |
  +--------------------+-------------------------------------+                            |
  |     10.253.10.0    | 00001010 11111101 00001010 00000000 | Le reseau cible est donc : |
  +--------------------+-------------------------------------+                            |
  |    255.240.0.0     | 11111111 11110000 00000000 00000000 |                            |
  +--------------------+-------------------------------------+----------------------------+
                       | 00001010 11110000 00000000 00000000 |         10.240.0.0         |
                       +-------------------------------------+----------------------------+
  
  Le masque de sous reseau est toujours le meme quoi qu'il arrive.


Afin de confirmer la connectivite entre les differents postes, on utilise la commande `ping` qui s'appuie sur le protocole ICMP.

Les responsables de secteur occupent un local situe a environ 15 km de l'hotel de ville. Il est decide de relier le reseau des responsables de secteur au reseaux principal de la mairie grace a une liaison specialisee a 64 Kbit/s. en reference a la grille tarrifaire ci-dessous, le cout sera de 208.85 E pour 10KM a cela s'ajoute 10.82E par kilometre. le coup total est donc de 262.85 E.
